# nettle-sys

Low-level Rust bindings for the Nettle cryptographic library (https://git.lysator.liu.se/nettle/nettle).

```toml
# Cargo.toml
[dependencies]
nettle-sys = "0.1"
```


# License

This project is licensed under either of

 * GNU General Public License, Version 2.0, ([LICENSE-GPL2](LICENSE-GPL2) or
   https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
 * GNU General Public License, Version 3.0, ([LICENSE-GPL3](LICENSE-GPL3) or
   https://www.gnu.org/licenses/gpl.html)
 * GNU Lesser General Public License, Version 3.0, ([LICENSE-LGPL3](LICENSE-LGPL3) or
   https://www.gnu.org/licenses/lgpl.html)

at your option.
